<!--

This template is for requesting work related to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/).

This is not the correct repository for requests related to docs.gitlab.com, the GitLab product, or other items.

For information regarding working on the about website please visit the [Brand and Digital Handbook](https://about.gitlab.com/handbook/marketing/brand-and-digital-design/)

You may link to this issue template using the following URL: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-work-request

-->

#### Briefly describe the request

<!--
Example: Create a landing page for a PPC lead generation campaign.
-->

#### What is/are the relevant URL(s)

<!--
Example: https://about.gitlab.com/etc/
-->

#### Has any name and/or keyword research occurred yet?

<!--
Example: https://gitlab.com/gitlab-com/www-gitlab-com/issues/1234567#89
-->

#### Please identify the directly responsible individual (DRI) and stakeholders

<!--
Who should we be asking for questions and reviews?

Example:
DRI: @username
Stakeholders: @user2, @user3, @user4...
-->

#### What team/dept/company initiatives does this relate to?

<!--
Examples:
* Brand and Digital wants to improve growth metrics via [tactical conversion MVCs](https://gitlab.com/groups/gitlab-com/-/epics/242).
* [OKR FY 21 Q1 CMO: 20% increase in inbound MQLs](https://about.gitlab.com/company/okrs/fy21-q1/)
-->

#### Is there a due date? If so, why?

<!--
Please note: Without sufficient lead time we will likely be unable to meet your deadline. Marketing web development is currently an impacted resource.

Example: Our due date is XXXX-XX-XX. We want to launch this page before a trade show happens on YYYY-YY-YY. We need a week of lead time to coordinate a press release before the event.
-->

#### What is the primary project goal?

<!--
Example: Increase self-hosted conversion rate.
-->

#### What are the other project goals?

<!--
Examples:
* Improve information discoverability.
* Reduce cognitive overload.
* Improve time-on-site.
-->

#### What information is this project trying to convey?

<!--
Example: Forrester says GitLab is great
-->

#### Who is the primary audience?

<!--
Example: potential event attendees.
-->

#### Who are the other audiences?

<!--
Examples:
* Speakers
* Media
* Potential sponsors
-->


#### Are there any pain-points related to this project?

<!--
Example:
* Users can't find what they're looking for.
* It's difficult to sign up.
* This feature is confusing.
* This template is difficult to update.
-->

#### What are the Key Performance Indicators (KPI)?

<!--
Examples:
* Email subscriptions
* Sponsorship info requests
* CFP submissions
* Attendee signups
* Scholarship requests
-->

#### What are the project must-haves?

<!--
This project cannot launch without:

(Example must-haves)
* Functional signup form
* CTA for proposals
-->

#### What are the project should-haves?

<!--
These are important but not necessary for delivery in the current timebox. These are often not as time-critical and may be delayed until a later milestone.

(Example should-haves)
* New look & feel
* Improved navigation
* Updated hero section
-->

#### What are the project could-haves?

<!--
These are desirable but not necessary.

(Example could-haves)
* Sliders
* Animations
* "Bells-and-whistles"
-->

#### What are the project wont-haves?

<!--
These are not planned at this time.

(Example wont-haves)
* A certain section of the page is currently performing well or functioning as intended. Do not change it.
* A requested feature is too expensive and not currently a good way to spend limited resources.
* A requested feature is impossible given current constraints and existing technology.
-->

#### Where do you expect traffic to come from?

<!--
Examples:
* Linked from https://about.gitlab.com/example/
* Social media campaign
* Targeted ads
-->

#### Will you need an opengraph image for social sharing?

<!--
Yes or no, and what could it look like?
-->

#### If relevant, please provide some example webpages for design guidance.

<!--
Please specify why you like each reference.
Examples:
* Pinterest implements recommendations well
* Stripe has good payment UX
* I like the aesthetics of nike.com
* Animation examples, microinteractions, etc
-->

#### If applicable, do you have any user journeys, prototypes, content/data?

<!--
Examples:
* Google docs/sheets, Figma prototypes, Mural diagrams.
-->

#### Please note any regression risks requiring validation before release

<!--
Examples:
- [ ] Do gated content forms still work?
- [ ] Does Cookiebot still work in CCR & GDPR?
- [ ] Do Marketo forms still present GDPR & Ukraine required fields?
-->

#### Please create a checklist of deliverables in this phase

<!--
Examples:
- [ ] Place & style marketo form
- [ ] Update page
- [ ] Approved review of changes
- [ ] Implement AB test
- [ ] Create heatmaps
- [ ] Release
- [ ] Evaluate results
- [ ] Code cleanup
-->

- [ ] Item 1
- [ ] Item 2
- [ ] Etc
- [ ] Create a followup issue for the next phase

<!--
#### Please apply your team's label using the dropdown menu below

Example: "Digital Marketing Programs"
-->


<!--THE END-->

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-request" ~"mktg-status::triage" ~"design-P4"
