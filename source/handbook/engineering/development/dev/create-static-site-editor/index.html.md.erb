---
layout: handbook-page-toc
title: Static Site Editor Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Static Site Editor Team is part of the [Dev](/handbook/engineering/development/dev/) Section and is responsible for enhancing the editing experience for static sites inside of GitLab.

## Team Members

The following people are permanent members of the Handbook Team:

<%= direct_team(role_regexp: /Create:Static Site Editor/, manager_role: 'Backend Engineering Manager, Create:Static Site Editor') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Static Site Editor)/, direct_manager_role: 'Backend Engineering Manager, Create:Static Site Editor') %>

## How to reach us

Depending on the context here are the most appropriate ways to reach out to the Static Site Editor group:
- GitLab epics/issue/MRs: `@gl-static-site-editor`
- Email: `static-site-editor@gitlab.com`
- Slack: `#g_create_static_site_editor` and `@static-site-editor-team`

## Projects

The team works primarily on the two projects listed below:

### GitLab
The work in [GitLab](https://gitlab.com/gitlab-org/gitlab/) revolves around enhancing the editing experience for static sites. 
See the [Static Site Editor](https://about.gitlab.com/direction/create/static_site_editor/) strategy for more information.

### Handbook Website
The work in the [GitLab Handbook](https://about.gitlab.com/handbook/) site is primarily focused on:
1. Reducing the time it takes from merge to deployment
1. Enhancing the editing, reading and sharing (think presenting a handbook page) functionality
1. Maintaining the integrity of the handbook site

See the [GitLab Handbook](https://about.gitlab.com/direction/create/gitlab_handbook/) strategy for more information.

#### about.gitlab.com Responsibility
The handbook is currently part of the larger [about.gitlab.com](https://gitlab.com/gitlab-com/www-gitlab-com/) website repository which in addition to the handbook includes the marketing website, blog, jobs etc.  

The repository is undergoing a refactor into a monorepo structure and the handbook will be split out into its own project. Progress can be followed here: [https://gitlab.com/groups/gitlab-com/-/epics/282](https://gitlab.com/groups/gitlab-com/-/epics/282)

## Work

In general, we use the standard GitLab [engineering workflow](/handbook/engineering/workflow/). To get in touch
with the Create:Static Site Editor team, it's best to create an issue in the relevant project
(typically [GitLab](https://gitlab.com/gitlab-org/gitlab/issues) or [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues)) and add the `~"group::static site editor"` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_static_site_editor](https://gitlab.slack.com/archives/g_create_static_site_editor) on Slack.

### Epic/Issue process

In the Static Site Editor group we use epics to plan out our category maturity roadmap and the user stories in it.

Static Site Editor category top-level epic: https://gitlab.com/groups/gitlab-org/-/epics/2688

Each maturity level is represented by an epic which holds further epics describing the user stories. 

A user story epic then consists out of issues that detail the implementation of the features.

#### Guidelines

- We try to break down user journey epics so that they can be delivered in a single release. 
- We collaborate at the epic level using comments to discuss the user journey requirements.
- We collaborate at the issue level using comments to discuss the feature requirements and implementation considerations.
- The PM is the DRI for user journey epics and any changes to the description of the user journey requires their approval.
- We do not require a 1:1 mapping between MRs and issues and instead encourage engineers to iterate to the requirements of an issue through multiple small MRs

#### Engineering planning process

The engineering team undertakes technical planning for each user story epic.  
Due to our iteration value we keep our planning process light, with the main aim of providing clarity around requirements and a high-level technical solution.

The expectation of the planning process is that by the end of it we:
1. Agree on the scope
1. Agree on the high-level technical approach
1. Clarify any unresolved matters
1. Identify initial implementation issues and their tasks
1. Identify which disciplines (frontend, backend) will need to work on the issues and whether it is suitable for multiple engineers to be assigned to it

##### The process
1. Requirements analysis
    1. The **Product Manager** alerts the group when a user story epic is ready for review.
    1. The team then reviews the requirements asynchronously to get an initial understanding of what will need to be implemented and to identify any gaps in the detail provided.
    1. The **Product Manager** incorporates the feedback by updating the epic description and requesting updates to any design mockups from the **Product Designer**.
1. Technical planning    
    1. Once the epic and assets have been updated the **Engineering Manager** will assign a **Planning DRI** to analyze the requirements and create an initial technical solution.
    1. The **Planning DRI** can be any engineer from the team.
    1. The **Planning DRI** will make a copy of the [Static Site Editor - Engineering Planning - Template](https://docs.google.com/document/d/1KS9qgK6jh_WwwPVXDYM9l9JQRn7W2PKz7GKQQQ14t7I/edit), and add their planning notes in it. This will be used as the basis of joint technical planning session.
    1. Once the initial planning is completed the **Planning DRI** schedules a synchronous call for the engineering team members to discuss the requirements and technical solution in a planning session.
    1. Out of this planning session might come actions for further research to be done and additional synchronous calls might be scheduled or the planning may continue asynchronously.
    1. It is the responsibility of the **Planning DRI** to ensure that the planning is completed.
    1. The final action of the **Planning DRI** is to indicate the implementation issues and their respective tasks.
        1. An implementation issues should ideally represent some user-facing value which is being delivered, and can be verified in the UI.
        1. The disciplines that are required to implement the issue should be indicated as well as whether the issue is suitable for multiple engineers to work on at the same time.
1. Issue creation
    1. The **Planning DRI** is responsible for creating the implementation issues identified in the planning.
        1. The issue description is filled with the notes, decisions and research coming out of the technical planning and the initial task list that was identified.
    1. The **Engineering Manager** will assign one or more team members to work on the issues informed by the indicated disciplines and suitability for multiple engineers working on it at the same time.
1. Development
    1. Once the issue is assigned to a team member(s) they become the DRI for implementing it.
    1. It is up the assignees to identify the iteration steps, interdependency between each other and integration of their work.

##### Planning cadence
The creation of user story epics, its validation and the creation of design mockups is an ongoing process that is not tied to a specific milestone. There could be many epics being worked on at any single point in time.

To encourage focus and not to overwhelm the engineering team with multiple priorities, technical planning will focus on one epic at a time, in priority order as determined by the **Product Manager**.

### Capacity planning

The Static Site Editor group does **NOT** make use of weights to rigidly plan capacity or track velocity.  
Rather a continuous delivery mentality is adopted, facilitated through using a Kanban-style work management approach to moving work through the product workflow process.

### What to work on

The primary source for things to work on are the development workflow boards:
- Handbook Category: [Development Workflow Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1625854?label_name[]=group%3A%3Astatic%20site%20editor)
- Static Site Editor Category: [Development Workflow Board](https://gitlab.com/gitlab-org/gitlab/-/boards/1624109?label_name[]=group%3A%3Astatic%20site%20editor)

#### What to work on first

Deliverables are considered top priority and are expected to be done by the end
of the iteration cycle in time for the release on the 22nd. To make it into the release in time 
it has to be merged and deployed to production by the 17th of the month.

These top priority issues are assigned to engineers by the Engineering Manager before the milestone begins.

Many things can happen that can result in a deliverable not actually being completed by the end of a milestone, 
and while this usually indicates that the Engineering Manager was too optimistic in their estimation
of the issue's complexity, or that an engineer's other responsibilities ended up
taking up more time than expected, this should never come as a surprise to the
Engineering Manager.

The sooner this potential outcome is anticipated and communicated, the more time
there is to see if anything can be done to prevent it, like reducing the scope
of the deliverable, or finding a different engineer who may have more time to
finish a deliverable that hasn't been started yet.

If this outcome cannot be averted and the deliverable ends up missing the
milestone, it will simply be moved to the next milestone to be finished up, and the
engineer and Engineering Manager will have a chance to
[retrospect](#retrospectives) and learn from what happened.

Generally, your deliverables are expected to take up about 75% of the
time you spend working in a month. The other 25% is set aside for other
responsibilities (code review, community merge request coaching, [helping
people out in Slack, participating in discussions in issues](/handbook/values/#collaboration),
etc), as well as urgent issues that come up during the month and need someone
working on them immediately (regressions, security issues, customer issues, etc).

#### What to work on next

If you have time to spare after finishing your deliverables and other
activities, you can pick any issue that is ready for development 
(i.e. is in the `workflow::ready for development` column).

The issues in this column are ordered by priority so choosing from the top of the list is recommended.

If anything is blocking you from getting started with the top issue immediately,
like unanswered questions or unclear requirements, you can skip it and consider
a lower priority issue, as long as you put your findings and questions in the
issue, so that the next engineer who comes around may find it in a better state.

Instead of picking up an issue from the Handbook or Static Site Editor boards, 
you may also choose to spend any spare time working on anything else that you believe 
will have a significant positive impact on the product or the company in general.
As the [general guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/general/) state, "we recognize that inspiration is
perishable, so if you’re enthusiastic about something that generates great
results in relatively little time feel free to work on that."

We expect people to be [managers of one](/handbook/values/#efficiency) and prefer responsibility
over rigidity, so there's no need to ask for permission if you
decide to work on something that's not on the issue board, but please keep your
other responsibilities in mind, and make sure that there is an issue, you are
assigned to it, and consider sharing it in [#g_create_static_site_editor](https://gitlab.slack.com/archives/g_create_static_site_editor).

### Workflow labels

The easiest way for engineering managers, product managers, and other stakeholders
to get a high-level overview of the status of all issues in the current milestone,
or all issues assigned to specific person, is through the Development Workflow boards,
which has columns for each of the workflow labels described on Engineering Workflow
handbook page under [Updating Issues Throughout Development](/handbook/engineering/workflow/#updating-issues-throughout-development).

As owners of the issues assigned to them, engineers are expected to keep the
workflow labels on their issues up to date, either by manually assigning the new
label, or by dragging the issue from one column on the board to the next.

### Issue labels

When creating an issue that relates to our group make sure to always add the following labels:
- Our group label: `group::static site editor`
- Category label:
    - `Category:Static Site Editor` for issues relating to the product we are building
    - `Category:GitLab Handbook` for issues relating to work we are doing on the GitLab Handbook in [gitlab-com/www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
- A discipline label (`frontend` and/or `backend`) if relevant to indicate which disciplines will need to be involved in actioning the issue.

The EM and PM is responsible for setting any other relevant labels and milestones on the issues.

### MR labels

All MRs¹ should contain the following labels to accurately reflect in our [metrics dashboard](https://app.periscopedata.com/app/gitlab/574876/Static-Site-Editor-Team-Metrics-Dashboard):
- Our group label: `group::static site editor`
- One of the following throughput categorization labels:
    - `feature` - when introducting new or updated functionality
    - `bug` - when fixing a bug
    - `backstage` - when doing behind the scenes work.
    - `security` - when fixing a security vulnerability

¹ Exception for `gitlab-com/www-gitlab-com` repo: Content related MRs (think fixing a spelling mistake, updating the info on the team page etc) should not contain the group label as it should not count towards our throughput.

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

### Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Project" retrospectives.

#### Per Milestone

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Static Site Editor", group_slug: 'static-site-editor' }) %>

#### Per Project

If a particular issue, feature, or other sort of project turns into a particularly useful learning experience, we may hold a synchronous or asynchronous retrospective to learn from it. If you feel like something you're working on deserves a retrospective:
1. [Create an issue](https://gitlab.com/gl-retrospectives/create-stage/static-site-editor/issues) explaining why you want to have a retrospective and indicate whether this should be synchronous or asynchronous
2. Include your EM and anyone else who should be involved (PM, counterparts, etc)
3. Coordinate a synchronous meeting if applicable

All feedback from the retrospective should ultimately end up in the issue for reference purposes.

### Deep Dives

<%= partial("handbook/engineering/development/dev/create/deep_dives.erb") %>

### Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Static Site Editor" }) %>

## Onboarding

New team members to the Static Site Editor group can use the resources below, in addition to their general onboarding, to get up to speed.

| Resource | Comments  | 
|---|---|
| [Product Section Direction - Dev](https://about.gitlab.com/direction/dev/) | Read the Product Direction for the Dev section to get a better understanding of the area of the product that we fall into |
| [Category Direction - Static Site Editor](https://about.gitlab.com/direction/create/static_site_editor/) | Read the Static Site Editor category direction to understand more about the product we are developing |
| [Static Site Editor Group Call Agenda](https://docs.google.com/document/d/1Hc1veBD1EfItCp8qCz3n4v6ttS47GphzubLTqkNCVqE/edit)  | Read the notes from our past group calls and watch the recordings to get an idea of what we discuss in our weekly sync call. Recording links are provided below each date heading and you can view the full list of recordings [here in Google Drive](https://drive.google.com/drive/search?q=static-site-editor-weekly%20mp4) |
| [🧠 Think Big : Static Site Editor](https://docs.google.com/document/d/1yruS9EY0TVMpdiaqz4E6a_D0g7vRGT53qO-7ir9u4OQ/edit#heading=h.25ukr6qi85ry) | We hold Think Big sessions to explore where we might want to take our category in the future. These sessions help inform what our vision looks like and the roadmap we take to getting there. |
| [Static Site Editor Vision Braindump](https://docs.google.com/document/d/1hMXxMw2YChL9l2CiurF8sffs01RDf1uzC4PppFQaQe4/edit#heading=h.h42owuydiis3) | If you're interested in all the various angles we explored in helping get to our current vision this document will give you some context. Note that its for context purposes only and not to be seen as a guide for what we will/wont do down the line. |

### Slack channels

The following channels are important to be a part of:
- [#g_create_static_site_editor](https://gitlab.slack.com/archives/CQ29L6B7T)
- [#g_create_static_site_editor_eng](https://gitlab.slack.com/archives/CU90JTCF8)
- [#g_create_static_site_editor_standup](https://gitlab.slack.com/archives/CUD52A59A)
- [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)
- [#handbook-escalation](https://gitlab.slack.com/archives/CVDP3HG5V)
- [#master-broken-www-gitlab-com](#https://gitlab.slack.com/archives/C413US389)

The following Slack channels, while optional, are relevant to our group and you should consider joining them:
- [#engineering-dev](https://gitlab.slack.com/archives/CG7FPF4KT)
- [#s_create](https://gitlab.slack.com/archives/CQ08Z46N9)
- [#g_create-fe](https://gitlab.slack.com/archives/CGPEQCL23)

### PTO Ninja

We use PTO Ninja to notify and delegate for planned timeoff. When setting up your integrations with Slack,
be sure to visit the PTO Ninja Home tab and choose the Calendar Sync option from the dropdown. 
Then add the team's shared Google Calendar `gitlab.com_56i46dodsa0mvtkfvn10hcssjo@group.calendar.google.com` in the "Additional Calendar to include?".

### Wiki

There is a [Wiki](https://gitlab.com/gitlab-com/create-stage/static-site-editor/-/wikis/home) for the team.  This is used to document information that may be more detailed, informal, or frequently changed, and thus may not be appropriate to be included in this handbook page.
